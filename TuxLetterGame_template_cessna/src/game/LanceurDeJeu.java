/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
/**
 *
 * @author idrisssagara
 */
public class LanceurDeJeu {

        
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException, Exception {

        // Declare un Jeu
        Jeu jeu; 
        
        //Instancie un nouveau jeu
        jeu = new Jeu("C:/Users/JTCessna/Desktop/TuxLetterGame_template_cessna/src/xml/profile.xml");
        
        //Execute le jeu
        jeu.jouer();
        
    }
}
